= Ruby + ADBC - A single API between Ruby and DBs

ADBC is Apache Arrow Database Connectivity. It provides a API that can connect to different databases by wrapping database specific APIs.

This is not a new approach. There are existing APIs such as Active Record, Sequel and ODBC. The difference between the existing APIs and ADBC is the focus on large data and performance.

ADBC is an important part to use Ruby for data processing. We can extract large data from many databases (not only RDBMSs but also data ware houses and so on) and load large data into many databases with ADBC. To use Ruby for data processing, we need data. ADBC helps it.

== License

=== Slide

CC BY-SA 4.0

Use the followings for notation of the author:

  * Sutou Kouhei

==== ClearCode Inc. logo

CC BY-SA 4.0

Author: ClearCode Inc.

It is used in page header and some pages in the slide.

==== ADBC related images

Apache-2.0

Author: The Apache Software Foundation

== For author

=== Show

  rake

=== Publish

  rake publish

== For viewers

=== Install

  gem install rabbit-slide-kou-rubykaigi-2023

=== Show

  rabbit rabbit-slide-kou-rubykaigi-2023.gem

